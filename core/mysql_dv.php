<?php
/**
 * Class mysql_dv is created to handling mysql database
 * All setings are in config file
 * 
 *
 * @author jacek gajc
 */
/**
 * @abstract Driver to mysql database
 * 
 * @package mysql_dv
 * @property string $error Error string. Last database or function error.
 * @property int $num number of afected or selected rows, last_id depending on question 
 */
class mysql_dv_core {
    private $debug;
    private $order_by;
    private $where;
    private $result;
    var $error;
    var $num;
/**
 * @return string Last database or or function error
 */    
    private function show_error(){
        if (($this->debug) and ($this->error != '')){
            echo $this->error;
        }
    }
/**
 * @abstract conetion to database, caling in create function
 */
    private function conect(){
        $this->error='';
        mysql_connect($GLOBALS['_CONFIG']['db_host'], $GLOBALS['_CONFIG']['db_user'], $GLOBALS['_CONFIG']['db_pass'], FALSE);
        mysql_select_db($GLOBALS['_CONFIG']['db_name']);
        $this->error.=mysql_error();
        mysql_query('SET NAMES utf8');
        $this->error.=mysql_error();
        $this->show_error();
    }
    
/**
 * 
 * @param boolean $debug if true, show errors. Defoult false
 */    
    public function __construct($debug=FALSE) {
        $this->debug=$debug;
        $this->error='';
        $this->order_by='';
        $this->conect();
    }
/**
 * 
 * @param string $sql database querry
 * @return resurce result of querry;
 */    
    
    public function qerry($sql){
        $result=mysql_query($sql);
        $this->error=mysql_error();
        $this->show_error();
        $this->num=mysql_num_rows($result);
        $this->result=$result;
        return $result;
    }
/**
 * 
 * @param string $table_name
 * @return resurce if where and order_by not set is result of querry SELECT * FROM $table_name
 */    
    public function get($table_name){
        $table_name= mysql_real_escape_string(($table_name));
        $sql='SELECT * FROM '.$table_name;
        if ($this->where != ''){
            $sql.= ' WHERE '.$this->where;
            $this->where='';
        }
        if ($this->order_by != ''){
            $sql.= ' ORDER BY '.$this->order_by;
            $this->order_by='';
        }
        $result=mysql_query($sql);
        $this->error=mysql_error();
        $this->show_error();
        $this->num=mysql_num_rows($result);
        $this->result=$result;
        return $result;
    }
/**
 * 
 * @param string $tablica table name
 * @param array $data table of data - first field is collumn name, second one value
 */        
    public function insert($tablica,$data){
        $keys=array_keys($data);
        $valu=array_values($data);
        for($i=0; $i<count($valu);$i++){
            $valu[$i]=  mysql_real_escape_string($valu[$i]);
        }
        $sql='INSERT INTO '.$tablica.' (`'.implode('`,`', $keys).'`) VALUES ("'.implode('","', $valu).'")';
        mysql_query($sql);
        $this->error=mysql_error();
        $this->show_error();
        $this->num=  mysql_insert_id();
    }
/**
 * 
 * @param string $tablica table name
 * @param array $data table of data - first field is collumn name, second one value
 */      
    public function update($tablica,$data){
        if ($this->where != ''){
            $keys=array_keys($data);
            $valu=array_values($data);
            for($i=0; $i<count($valu);$i++){
                $valu[$i]=  mysql_real_escape_string($valu[$i]);
            }
            for($i=0; $i<count($keys);$i++){
                $keys[$i]=' `'.$keys[$i].'`="'.$valu[$i].'"';
            }
            $set=  implode(', ', $keys);
            $sql= 'UPDATE '.$tablica.' SET '.$set.' WHERE '.$this->where;
            mysql_query($sql);
            $this->num=  mysql_affected_rows();
            $this->error=mysql_error();
            $this->where='';
        }else{
            $this->error='Empty WHERE statment ';
        }
        $this->show_error();
    }
/**
 * @abstract deleting record indecatet in WHERE statment
 * 
 * @param string $tablica name of table
 */    
    public function delete($tablica){
        if ($this->where != ''){
            $sql= 'DELETE FROM '.$tablica.' WHERE '.$this->where;
            mysql_query($sql);
            $this->num=  mysql_affected_rows();
            $this->error=mysql_error();
            $this->where='';
        }else{
            $this->error='Empty WHERE statment ';
        }
        $this->show_error();
    }
/**
 * 
 * @param string $where value of WHERE part of statment
 */    
    public function set_where($where){
        $this->where=$where;
    }
 
 /**
  * 
  * @param string $order value of ORDER BY part of statment
  */
    public function set_order($order){
        $this->order_by=$order;
    }
    
/**
 * @abstract convert result of last SELECT qerry to assoc array
 * @return array 
 */
    public function asoc_array(){
        print_r($this->result);
        $result=array();
        while ($row = mysql_fetch_assoc($this->result)) {
            $result[]=$row;
        }
        return $result;
    }

}

?>
