<?php
/**
 *Main project file
 * including __autolad function and runing classes 
 *@author j.g.
 */
include_once SERVER_ROOT.'/config/config.php';


function __autoload($className)
{
	//parse out filename where class should be located
	list($filename , $suffix) = split('_' , $className);
	//Defoult take from controlers
        $folder='/controlers/';
	//select the folder where class should be located based on suffix
	switch (strtolower($suffix))
	{	
		case 'model':
			$folder = '/models/';
		break;
		case 'library':
			$folder = '/libraries/';
		break;
		case 'core':
			$folder = '/core/';
		break;
	}

	//compose file name
	$file = SERVER_ROOT . $folder . strtolower($filename) . '.php';
	//fetch file
	if (file_exists($file))
	{
		//get file
		include_once($file);		
	}
	else
	{
		//file does not exist!
		die("File '$filename' containing class '$className' not found in '$folder'.");	
	}
}

$request = $_SERVER['QUERY_STRING'];
if ($request==''){
    $request=DEFAULT_CON;
}
$querry=  explode('/', $request);
$target = SERVER_ROOT . '/controllers/' . $querry[0] . '.php';

if (file_exists($target))
{
	include_once($target);
	//modify page to fit naming convention
	$class = $querry[0];
	if (class_exists($class))
	{
		$controller = new $class;
	}
	else
	{
		//did we name our class correctly?
		die('class does not exist!');
	}
}
else
{
	//can't find the file in 'controllers'! 
	die('404');
}

if (isset($querry[1])){
    $controller->$querry[1]($querry[2],$querry[3],$querry[4],$querry[5],$querry[6]);
}else{
    $controller->index();
}