<?php
/**
 * Class to open, evaluate and display tpl files
 *
 *@author jg
 *@package tplen
 * 
 */

class tplen_core {
    protected $source = FALSE;
    protected $data;

/**
 * 
 * Open temptate file
 * @param string $tpl_file file with temptate
 */
    public function tpl_file($tpl_file){
        $tpl_file =$GLOBALS['_CONFIG']['tpl_path'].$tpl_file;
        $this->data=array();
        if (file_exists($tpl_file)){
            $this->source=$tpl_file;
        }else{
            echo 'File '.$tpl_file.' not found';
        }
    }
    
/**
 * 
 * @param string $name name of variable to send to temptate
 * @param open $var value of variable to send to temptate
 */    
    public function set($name,$var){
        $this->data[$name]=$var;
    }

/**
 * display or send to variable parsed temptate with callig variables ($data)
 * @param boolean $to_var if false - dispalay, else send to varaible
 * @return string if $to_var true consist parsed temptate
 */    
    public function show($to_var=FALSE){
        extract($this->data);
        ob_start();
        include $this->source;
        $result=  ob_get_contents();
        ob_end_clean();
        if (! $to_var){
            echo $result;
        } else {
            return $result;  
        }
        
    }
}

?>
