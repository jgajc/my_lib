<?php
/**
 *@var array $_CONFIG Main configuration array
 *@author j.g.
 */

$_CONFIG=  array();
//Temptate engine part
$_CONFIG['tpl_path']='./tpl/';
//Database (MySQL) part
$_CONFIG['db_host']='localhost';
$_CONFIG['db_name']='test';
$_CONFIG['db_user']='root';
$_CONFIG['db_pass']='root';
