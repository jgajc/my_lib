<?php
/**
 *Sample menu config file
 *
 *@author j.g.
 */
$menu[3]=
array(
    'id'=>4,
    'teskt'=>'T menu 2',
    'parent'=>0,
    'link'=>'clasa1/metoda2/param1',
    'order'=>2
);


$menu[0]=
array(
    'id'=>1,
    'teskt'=>'T menu 1',
    'parent'=>0,
    'link'=>'clasa1/metoda1/param1',
    'order'=>1
);
$menu[1]=
array(
    'id'=>2,
    'teskt'=>'S1 menu 1',
    'parent'=>1,
    'link'=>'clasa1/metoda1/param1',
    'order'=>1
);
$menu[2]=
array(
    'id'=>3,
    'teskt'=>'S1 menu 2',
    'parent'=>1,
    'link'=>'clasa1/metoda1/param2',
    'order'=>2
);

$menu[4]=
array(
    'id'=>5,
    'teskt'=>'S2 menu 1',
    'parent'=>4,
    'link'=>'clasa1/metoda2/param1',
    'order'=>1
);
$menu[5]=
array(
    'id'=>6,
    'teskt'=>'S2 menu 2',
    'parent'=>4,
    'link'=>'clasa1/metoda2/param2',
    'order'=>2
);